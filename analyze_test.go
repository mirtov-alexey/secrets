package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func mockCLI(flags map[string]string) *cli.Context {
	app := cli.NewApp()
	app.Commands = command.NewCommands(command.Config{
		Match:        func(path string, info os.FileInfo) (bool, error) { return true, nil },
		Convert:      func(r io.Reader, p string) (*report.Report, error) { return &report.Report{}, nil },
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
	})
	set := flag.NewFlagSet("testFlagSet", 0)
	for k, v := range flags {
		set.String(k, v, "")
	}
	return cli.NewContext(app, set, nil)
}

func TestArgs(t *testing.T) {
	tests := []struct {
		c                  *cli.Context
		commitsFileContent []byte
		commitsFile        string
		want               []string
	}{
		{
			c: mockCLI(map[string]string{
				flagCommits: "c1,c2,c3,c4",
			}),
			want: []string{"--log-opts", "--no-merges c4^..c1"},
		},
		{
			c: mockCLI(map[string]string{
				flagCommitsFile: "commitFileTest",
			}),
			commitsFile:        "commitFileTest",
			commitsFileContent: []byte("c1\nc2\nc3\nc4"),
			want:               []string{"--log-opts", "--no-merges c4^..c1"},
		},
		{
			c:    mockCLI(map[string]string{}),
			want: []string{"--no-git"},
		},
		{
			c: mockCLI(map[string]string{
				flagHistoricScan: "true",
			}),
			want: []string{},
		},
		{
			c: mockCLI(map[string]string{
				flagCommitFrom: "c4",
				flagCommitTo:   "c1",
			}),
			want: []string{"--log-opts", "--no-merges c4^..c1"},
		},
	}

	for _, test := range tests {
		if test.commitsFile != "" {
			if err := ioutil.WriteFile(test.commitsFile, test.commitsFileContent, 0644); err != nil {
				t.Error(err)
			}
		}
		got, err := appendScanTypeArgs(test.c, []string{})
		if err != nil {
			t.Fatal(err)
		}

		assert.Equal(t, got, test.want)
	}
}

func TestConfigPath(t *testing.T) {
	rootPath := "/root/path"
	tests := []struct {
		name string
		in   *ruleset.Config
		want string
	}{
		{
			name: "nil ruleset",
			in:   nil,
			want: defaultPathGitleaksConfig,
		},
		{
			name: "passthrough without a matching target",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:  ruleset.PassthroughFile,
						Value: "gitleaks-config.toml",
					},
				},
			},
			want: defaultPathGitleaksConfig,
		},
		{
			name: "Passthrough File",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughFile,
						Target: "gitleaks.toml",
						Value:  "gitleaks-config.toml",
					},
				},
			},
			want: filepath.Join(rootPath, "gitleaks-config.toml"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path, err := configPath(rootPath, tt.in)

			if err != nil {
				t.Errorf("Expected a nil err, but got: %v", err)
			}

			assert.Equal(t, path, tt.want)
		})
	}
}

func TestConfigPathWithPassthroughRaw(t *testing.T) {
	rawGitleaksConfig := "the most raw"
	rs := &ruleset.Config{
		Passthrough: []ruleset.Passthrough{
			{
				Type:   ruleset.PassthroughRaw,
				Target: "gitleaks.toml",
				Value:  rawGitleaksConfig,
			},
		},
	}
	path, err := configPath("/root/path", rs)

	if err != nil {
		t.Errorf("Expected a nil err, but got: %v", err)
	}

	content, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("tried to read %s, but got %v", path, err)
	}

	assert.Equal(t, string(content), rawGitleaksConfig)
}

func TestAddEntropyRule(t *testing.T) {
	tmpFile, err := ioutil.TempFile("", "config.toml")
	if err != nil {
		t.Fatal(err)
	}
	if err := addEntropyRule(5.0, tmpFile.Name()); err != nil {
		t.Fatal(err)
	}
	b, err := ioutil.ReadAll(tmpFile)
	if err != nil {
		t.Fatal(err)
	}
	want := fmt.Sprintf(entropyRuleTemplate, 5.0)
	assert.Equal(t, string(b), want)

}
